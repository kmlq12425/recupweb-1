/* 
    Desarrollar una función en Javascript que permita recorrer los números del 1 al 100 y contar cuantos múltiplos de 7 existen entre el 1 y el 100. Poner el código en la respuesta Y enviar el enlace de GitLab al correo institucional patricia.quiroz@uleam.edu.ec *
*/

let totalMultiplos = 0;

for (let i = 1; i<=100; i++) {
    if(i % 7 === 0 ) totalMultiplos++;
}

console.log(totalMultiplos);