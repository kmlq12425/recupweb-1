/* 
    Crear una función JavaScript que reciba un número de cédula y actualice el teléfono de ese cliente en la base de datos de clientes y en la base de datos de pedidos. Previamente validar que sea un dato numérico. Poner el código en la respuesta Y enviar el enlace de GitLab al correo institucional patricia.quiroz@uleam.edu.ec 
*/

const updatePhone = async (cedula, phone) => {
    if(!/[0-9]{10}/.test(cedula)) throw new Error('No es una cedula valida');

    if(typeof cedula !== 'number') throw new Error('El teléfono no es un número');
    if(typeof phone !== 'number') throw new Error('El teléfono no es un número');

    database.query('UPDATE user SET ? WHERE `user_cedula` = ?', [
        {
            phone
        }, cedula
    ], (error) => {
        if (error) throw error;
    })
}